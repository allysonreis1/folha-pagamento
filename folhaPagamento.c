/*Aluno: Allyson Reis Vieira de Aguiar <allysonreis1@gmail.com> Matr�cula 16133686
  Aluno: Paulo Henrique Duarte da Silva <duartepaulohenrique50@gmail.com> Matr�cula 16133057
  Disciplina: Computa��o II FACEMA - Professor Hilson
  
  Reposit�rio: https://gitlab.com/allysonreis1/folha-pagamento
  Este software tem todos os direitos reservados. De propriedade � Allyson Reis Vieira de Aguiar e Paulo Henrique Duarte da Silva.
*/

#include<stdio.h>
#include<string.h>
#include<locale.h> //Biblioteca para uso da fun��o setlocale();
#define MAX 100 //Criando uma constante

FILE *arquivo; //Fun��o para criar ou abrir um arquivo bin�rio
void abreArquivo (void) {
	arquivo = fopen("arquivo.bin", "r+b");
	if (arquivo == NULL) {
		arquivo = fopen("arquivo.bin", "w+b");
	}
}

//Declara��o de Fun��es
void menuInicial (void);
void cadastrar (void);
void consultar (void);
void imprimir (void);
float calculoINSS(float salario);
float calculoFGTS (float salario);
float calculoIRRF (float salario, float inss);
float calculoSalarioFamilia (float salario, int totalFilhos);
float calculoHorasExtras (float salario, float horasTrabalhadas, float quantidadeHoraExtra);
float calculoGratificacao (float salario, char possuiGratificacao, int numeroGratificacao);
float calculoSalarioBruto (float salarioBase, float valorHorasExtras, float gratificacao, float salarioFamilia, float desconto);
float calculoDesconto (float inss, float horasFaltas, float irrf);
float calculoSalarioLiquido (float salarioBruto, float descontos);
float calculoHorasFaltas (float salarioBase, float quantidadeHorasFaltas, float horasTrabalho);

//Declra��o da Estrutura para um funcion�rio
struct funcionario {
	int matricula;
	int dia, mes, ano; //Data de admiss�o
	int diaPagamento, mesPagamento, anoPagamento;
	int quantidadeFilhos;
	int numeroGratificacoes;
	char nome[MAX];
	char empresa[MAX];
	char cargo[MAX];
	char possuiFilho;
	char possuiFilhoInvalido;
	char possuiGratificacao;
	char possuiFilho14;
	char possuiPlanoSaude;
	float horaExtra;
	float horaFaltando;
	float horaTrabalhada;
	float salarioBase;
	float valorINSS;
	float valorFGTS;
	float valorSalarioFamilia;
	float valorHorasExtras;
	float valorGratificacao;
	float salarioBruto;
	float valorDesconto;
	float salarioLiquido;
	float valorHorasFaltas;
	float valorIRRF;
	float comissao;
	float valorPlanoSaude;
};
struct funcionario umFuncionario, funcionarioCadastrado;

void exibir (struct funcionario exibeFuncionario); //Fun��o declarada aqui pois usa elementos da struct. Evita erros

main () {
	setlocale(LC_ALL, "Portuguese"); //Determina o idioma 
	
	int opcao;
	
	do { 
		abreArquivo();
		menuInicial();
		scanf("%d", &opcao);
		
		switch(opcao) {
			case 1 : 
				cadastrar();
				system("cls");//limpa a tela
			break;
			case 2 :
				consultar();
				system("cls");
			break;
			case 3 :
				imprimir();
				system("cls");
			break;
			case 4 :
				fclose(arquivo);
				system("cls");
				printf("\t\tOBRIGADO!\n");
				getch();
			break;
			default:
				printf("\nOp��o incorreta! Tente novamente!\n");
				getch();
				system("cls");
		}
	} while (opcao != 4);
}

void menuInicial (void) {
	printf("\t------------------------------------------------------\n");
	printf("\t|           GEST�O DE FOLHA DE PAGAMENTO             |\n");
	printf("\t------------------------------------------------------\n");
	printf("\t|        1 - CADASTRAR  |      3 - IMPRIMIR          |\n");
	printf("\t------------------------------------------------------\n");
	printf("\t|        2 - CONSULTAR  |      4 - SAIR              |\n");
	printf("\t------------------------------------------------------\n");
	printf("\t|           INFORME A OPERA��O DESEJADA              |\n");
	printf("\t------------------------------------------------------\n");
	printf("\t         %c copyright(All Rights Reserved)\n", 169);
}

void cadastrar (void) {
	int str_size;
	
	system("cls"); //Limpa a tela quando executado
	
	fflush(stdin);//limpa o buffer do teclado
	
	printf("Digite o nome do funcion�rio: ");
	fgets(umFuncionario.nome, MAX, stdin);
	
	str_size = strlen(umFuncionario.nome); //Verifica o tamanho da string
	
	if (umFuncionario.nome[str_size-1] == '\n') { //Remove o '\n' da string e adiciona o '\0' no lugar
		umFuncionario.nome[str_size-1] = '\0';
	}
	
	printf("Digite a matr�cula do funcion�rio: ");
	scanf("%d", &umFuncionario.matricula);
	fflush(stdin);//limpa o buffer do teclado
	
	printf("Digite o nome da Empresa: ");
	fgets(umFuncionario.empresa, MAX, stdin);
	
	str_size = strlen(umFuncionario.empresa);
	
	if (umFuncionario.empresa[str_size-1] == '\n') {
		umFuncionario.empresa[str_size-1] = '\0';
	}

	
	printf("Insira a data de admiss�o(dd/mm/aaaa): ");
	scanf("%d/%d/%d", &umFuncionario.dia, &umFuncionario.mes, &umFuncionario.ano);
	fflush(stdin);//limpa o buffer do teclado
	
	printf("Insira o cargo do funcion�rio: ");
	fgets(umFuncionario.cargo, MAX, stdin);
	
	str_size = strlen(umFuncionario.cargo);
	
	if (umFuncionario.cargo[str_size-1] == '\n') {
		umFuncionario.cargo[str_size-1] = '\0';
	}	
	
	printf("Digite a quantidade de horas que o funcionario trabalha por mes: ");
	scanf("%f", &umFuncionario.horaTrabalhada);
	
	printf("Digite o saldo de horas extras: ");
	scanf("%f", &umFuncionario.horaExtra);
	
	printf("Digite o saldo devedor de horas: ");
	scanf("%f", &umFuncionario.horaFaltando);
	fflush(stdin);//limpa o buffer do teclado
	
	printf("Possui plano de sa�de(S/N): ");
	scanf("%c", &umFuncionario.possuiPlanoSaude);
	fflush(stdin);
	
	if (umFuncionario.possuiPlanoSaude == 'S' || umFuncionario.possuiPlanoSaude == 's') {
		printf("Digite o valor do plano de sa�de: ");
		scanf("%f", &umFuncionario.valorPlanoSaude);
		fflush(stdin);
	} else {
		umFuncionario.possuiPlanoSaude = 'N';
		umFuncionario.valorPlanoSaude = 0;
	}
	
	printf("Possui filho (S/N): ");
	scanf("%c", &umFuncionario.possuiFilho);
	fflush(stdin);//limpa o buffer do teclado
	
	if (umFuncionario.possuiFilho == 'S' || umFuncionario.possuiFilho == 's') {
		printf("Possui filho com algum tipo de invalidez: ");
		scanf("%c", &umFuncionario.possuiFilhoInvalido);
		fflush(stdin);//limpa o buffer do teclado
		
		printf("Possui filho menor de 14 anos(S/N): ");
		scanf("%c", &umFuncionario.possuiFilho14);
		fflush(stdin);
		
		printf("Total de filhos-> menores que 14 anos + filhos invalidos: ");
		scanf("%d", &umFuncionario.quantidadeFilhos);
		fflush(stdin);
	} else {
		umFuncionario.possuiFilhoInvalido = 'N';
		umFuncionario.possuiFilho14 = 'N';
		umFuncionario.quantidadeFilhos = 0;
	}
	
	printf("Possui gratifica��o? (S/N) ");
	scanf("%c", &umFuncionario.possuiGratificacao);
	fflush(stdin);//limpa o buffer do teclado
	
	if (umFuncionario.possuiGratificacao == 'S' || umFuncionario.possuiGratificacao == 's') {
		printf("Digite a quantidade de gratifica��es: ");
		scanf("%d", &umFuncionario.numeroGratificacoes);
		fflush(stdin);
	} else {
		umFuncionario.numeroGratificacoes = 0;
		umFuncionario.possuiGratificacao = 'N';
	}
	
	printf("Digite o sal�rio base do funcion�rio: ");
	scanf("%f", &umFuncionario.salarioBase);
	fflush(stdin);
	
	printf("Digite o valor da comissao: ");
	scanf("%f", &umFuncionario.comissao);
	fflush(stdin);
	
	printf("Insira a data do pagamento(dd/mm/aaaa): ");
	scanf("%d/%d/%d", &umFuncionario.diaPagamento, &umFuncionario.mesPagamento, &umFuncionario.anoPagamento);
	fflush(stdin);//limpa o buffer do teclado
	
	//Faz os C�culos e armazena na struct
	umFuncionario.valorINSS = calculoINSS(umFuncionario.salarioBase);
	umFuncionario.valorFGTS = calculoFGTS(umFuncionario.salarioBase);
	umFuncionario.valorSalarioFamilia = calculoSalarioFamilia(umFuncionario.salarioBase, umFuncionario.quantidadeFilhos);
	umFuncionario.valorHorasExtras = calculoHorasExtras(umFuncionario.salarioBase, umFuncionario.horaTrabalhada, umFuncionario.horaExtra);
	umFuncionario.valorGratificacao = calculoGratificacao(umFuncionario.salarioBase, umFuncionario.possuiGratificacao, umFuncionario.numeroGratificacoes);
	umFuncionario.valorHorasFaltas = calculoHorasFaltas(umFuncionario.salarioBase,umFuncionario.horaFaltando, umFuncionario.horaTrabalhada);
	umFuncionario.valorIRRF = calculoIRRF(umFuncionario.salarioBase, umFuncionario.valorINSS);
	umFuncionario.valorDesconto = calculoDesconto(umFuncionario.valorINSS, umFuncionario.valorHorasFaltas, umFuncionario.valorIRRF);
	umFuncionario.salarioBruto = calculoSalarioBruto(umFuncionario.salarioBase, umFuncionario.valorHorasExtras, umFuncionario.valorGratificacao, umFuncionario.valorSalarioFamilia, umFuncionario.valorDesconto);
	umFuncionario.salarioLiquido = calculoSalarioLiquido(umFuncionario.salarioBruto, umFuncionario.valorDesconto);
	
	fseek(arquivo, 0, SEEK_END);//Posiciona o cursor no fim do arquivo. Para que n�o salve em cima de outra struct
	fwrite(&umFuncionario, sizeof(struct funcionario), 1, arquivo); //Escreve a struct umFuncionario no arquivo
	fclose(arquivo); //Fecha o arquivo
	puts("\nPressione ENTER para concluir o cadastro!\n");
	getch();
}

void consultar (void) {
	char auxiliarBusca[MAX];
	int str_size;
	
	printf("Digite o nome completo do funcion�rio: ");
	fflush(stdin);
	fgets(auxiliarBusca, MAX, stdin);
	
	str_size = strlen(auxiliarBusca);
	if (auxiliarBusca[str_size-1] == '\n'){
		auxiliarBusca[str_size-1] = '\0';
	}
	
	if (arquivo == NULL) {
		printf("\nN�o h� funion�rios cadastrados!\n");
	} else {
		fseek(arquivo, 0, SEEK_SET); //Posiciona o ponteiro no inicio do arquivo
		while (!feof(arquivo)) { //Enquanto n�o chegar no fim do arquivo
			fread(&funcionarioCadastrado, sizeof(struct funcionario), 1, arquivo);//Ler as struct's salvas dentro do arquivo
			if (strstr(funcionarioCadastrado.nome, auxiliarBusca) != NULL && feof(arquivo) == 0) {
				printf("Nome: %s\n", funcionarioCadastrado.nome);
				printf("Matr�cula: %d\n", funcionarioCadastrado.matricula);
				printf("Empresa: %s\n", funcionarioCadastrado.empresa);
				printf("Data de Admiss�o: %.2d/%.2d/%d\n", funcionarioCadastrado.dia, funcionarioCadastrado.mes, funcionarioCadastrado.ano);
			}
		}
	}
	fclose(arquivo);
	getch();
}

void imprimir (void) {
	char auxiliarBusca[MAX];
	char auxiliarNome[MAX];
	char extensao[4] = ".txt";
	FILE *arquivoAuxiliar;
	
	printf("Digite o nome do funcion�rio: ");
	fflush(stdin);
	fgets(auxiliarBusca, MAX, stdin);
	
	if(arquivo == NULL){
		printf("\nN�o h� funion�rios cadastrados!\n");
	} else {
		fseek(arquivo, 0, SEEK_SET);
		
		while (!feof(arquivo)) {
			
			fread(&funcionarioCadastrado, sizeof(struct funcionario), 1, arquivo);
			
			if (strstr(auxiliarBusca, funcionarioCadastrado.nome) != NULL && feof(arquivo) == 0) {
				strcpy(auxiliarNome, funcionarioCadastrado.nome);
				strcat(auxiliarNome, extensao); //Concatena o nome do funcion�rio � extens�o txt. Garante que o arquivo ser� salvo com o nome do Funcion�rio
				arquivoAuxiliar = fopen(auxiliarNome, "w"); //Abre o arquivo para escrita (w)
				
				//Escreve no arquivo
				fprintf(arquivoAuxiliar, "\n_________________________________________________________\n");
				fprintf(arquivoAuxiliar, "|Informativo de Contra-Cheque     Empresa: %s       |\n",funcionarioCadastrado.empresa);
				fprintf(arquivoAuxiliar, "|Matr�cula: %d                                    |\n", funcionarioCadastrado.matricula);
				fprintf(arquivoAuxiliar, "|Nome: %s                                            |\n", funcionarioCadastrado.nome);
				fprintf(arquivoAuxiliar, "|Cargo: %s                                         |\n", funcionarioCadastrado.cargo);
				fprintf(arquivoAuxiliar, "|_______________________________________________________|\n");
				fprintf(arquivoAuxiliar, "|Data do pagamento: %02d/%02d/%d                          |\n", funcionarioCadastrado.diaPagamento, funcionarioCadastrado.mesPagamento, funcionarioCadastrado.anoPagamento);
				fprintf(arquivoAuxiliar, "|_______________________________________________________|\n");
				fprintf(arquivoAuxiliar, "|Cod       Evento              Ref.           Valor     |\n");
				fprintf(arquivoAuxiliar, "|-------------------------------------------------------|\n");
				fprintf(arquivoAuxiliar, " 1         Hora Normal          %.0f           %.2f      \n", funcionarioCadastrado.horaTrabalhada, funcionarioCadastrado.salarioBase);
				fprintf(arquivoAuxiliar, " 2         Hora Extra          %.0f              %.2f        \n", funcionarioCadastrado.horaExtra, funcionarioCadastrado.valorHorasExtras);
				fprintf(arquivoAuxiliar, " 3         Comiss�o            0               %.2f        \n", funcionarioCadastrado.comissao);
				fprintf(arquivoAuxiliar, " 4         Gratifica��o        %d              %.2f        \n", funcionarioCadastrado.numeroGratificacoes, funcionarioCadastrado.valorGratificacao);
				fprintf(arquivoAuxiliar, " 5         Hora Falta          %.0f              %.2f        \n", funcionarioCadastrado.horaFaltando, funcionarioCadastrado.valorHorasFaltas);
				fprintf(arquivoAuxiliar, " 6         INSS                10              %.2f        \n", funcionarioCadastrado.valorINSS);
				fprintf(arquivoAuxiliar, " 7         Plano de Sa�de      %c               %.2f       \n", funcionarioCadastrado.possuiPlanoSaude, funcionarioCadastrado.valorPlanoSaude);
				fprintf(arquivoAuxiliar, "---------------------------------------------------------\n");
				fprintf(arquivoAuxiliar, "Total de proventos............ %f\n", funcionarioCadastrado.salarioBruto);
				fprintf(arquivoAuxiliar, "Total de descontos............ %f\n", funcionarioCadastrado.valorDesconto);
				fprintf(arquivoAuxiliar, "Liquido a receber............. %f\n", funcionarioCadastrado.salarioLiquido);
				fprintf(arquivoAuxiliar, "_________________________________________________________\n");
				fprintf(arquivoAuxiliar, "|Pag. 1          Compet�ncia: %d/%d                      |\n", funcionarioCadastrado.mesPagamento, funcionarioCadastrado.anoPagamento);
				fprintf(arquivoAuxiliar, "|_______________________________________________________|\n");
			}
		}
	}
	exibir(funcionarioCadastrado); //Chamada da fun��o para exibir as informa��es na tela
	fclose(arquivoAuxiliar);
	fclose(arquivo);
}


float calculoINSS (float salario) {
	float inss;
	
	if (salario <= 1247.70) {
		inss = salario*0.08;
	} else if (salario >= 1247.71 && salario <= 2079.50) {
		inss = salario*0.09;
	} else if (salario >= 2079.51 && salario <= 4159.00) {
		inss = salario*0.11;
	}
	
	return inss;
}

float calculoFGTS (float salario) {
	return salario*0.08;
}

float calculoIRRF (float salarioBase, float inss){
	float irrf = 0;
	//float salarioBase;
	
	//salario base para c�lculo do irrf
	//salarioBase = salario-inss;
	
	if (salarioBase <= 1710.78) {
		irrf = salarioBase*1;	
	} else if (salarioBase >= 1710.79 && salarioBase  <= 2563.91) {
		irrf = (salarioBase*0.075) - 128.31;
	} else if (salarioBase >= 2563.92 && salarioBase <= 3418.59) {
		irrf = (salarioBase*0.15) - 320.60;
	} else if (salarioBase >= 3418.60 && salarioBase <= 4271.59) {
		irrf = (salarioBase*0.225) - 577.00;
	} else if (salarioBase > 4271.59) {
		irrf = (salarioBase*0.275) - 790.58;
	}
	
	return irrf;
}

float calculoSalarioFamilia (float salario, int totalFilhos) {
	float salarioFamilia = 0;
	
	if (salario >= 646.55 && salario <= 971.78) {
		salarioFamilia = (float) totalFilhos*23.36;
	}
	
	return salarioFamilia;
}

float calculoHorasExtras (float salario, float horasTrabalhadas, float quantidadeHoraExtra) {
	float salarioHora = 0, valorHoraExtra = 0;
	
	//Calcula o valor do sal�rio hora
	salarioHora = salario/horasTrabalhadas;
	
	//Valor da hora extra. � o valor do sal�rio hora acrescido 50% multiplicado pela quantidade de hora extra trabalhada
	valorHoraExtra = (salarioHora*1.5)*quantidadeHoraExtra; 
	
	return valorHoraExtra;
}

float calculoGratificacao (float salario, char possuiGratificacao, int numeroGratificacao) {
	float gratificacao = 0;
	
	while (numeroGratificacao > 0) {
		if (possuiGratificacao == 'S' || possuiGratificacao == 's') {
			gratificacao = gratificacao + salario*0.2;
		}
		numeroGratificacao--;
	}
	return gratificacao;
}

float calculoSalarioBruto (float salarioBase, float valorHorasExtras, float gratificacao, float salarioFamilia, float desconto) {
	return salarioBase + valorHorasExtras + gratificacao + salarioFamilia + desconto;
}

float calculoDesconto (float inss, float horasFaltas, float irrf) {
	return inss + horasFaltas + irrf;
}

float calculoSalarioLiquido (float salarioBruto, float descontos) {
	return salarioBruto - descontos;
}

float calculoHorasFaltas (float salarioBase, float quantidadeHorasFaltas, float horasTrabalho) {
	float valorHora, total;
	
	valorHora = salarioBase/horasTrabalho;
	total = valorHora*quantidadeHorasFaltas;
	
	return total;
}

void exibir (struct funcionario exibeFuncionario) {
	system("cls");
	printf("\n_________________________________________________________\n");
	printf("|Informativo de Contra-Cheque     Empresa: %s       |\n",exibeFuncionario.empresa);
	printf("|Matr�cula: %d                                    |\n", exibeFuncionario.matricula);
	printf("|Nome: %s                                            |\n", exibeFuncionario.nome);
	printf("|Cargo: %s                                         |\n", exibeFuncionario.cargo);
	printf("|_______________________________________________________|\n");
	printf("|Data do pagamento: %02d/%02d/%d                          |\n", exibeFuncionario.diaPagamento, exibeFuncionario.mesPagamento, exibeFuncionario.anoPagamento);
	printf("|_______________________________________________________|\n");
	printf("|Cod       Evento              Ref.           Valor     |\n");
	printf("|-------------------------------------------------------|\n");
	printf(" 1         Hora Normal         %.0f           %.2f      \n", funcionarioCadastrado.horaTrabalhada, funcionarioCadastrado.salarioBase);
	printf(" 2         Hora Extra          %.0f              %.2f        \n", exibeFuncionario.horaExtra, exibeFuncionario.valorHorasExtras);
	printf(" 3         Comiss�o            0               %.2f        \n", exibeFuncionario.comissao);
	printf(" 4         Gratifica��o        %d              %.2f        \n", exibeFuncionario.numeroGratificacoes, exibeFuncionario.valorGratificacao);
	printf(" 5         Hora Falta          %.0f              %.2f        \n", exibeFuncionario.horaFaltando, exibeFuncionario.valorHorasFaltas);
	printf(" 6         INSS                10              %.2f        \n", exibeFuncionario.valorINSS);
	printf(" 7         Plano de Sa�de      %c               %.2f       \n", exibeFuncionario.possuiPlanoSaude, exibeFuncionario.valorPlanoSaude);
	printf("---------------------------------------------------------\n");
	printf("Total de proventos............ %.2f\n", exibeFuncionario.salarioBruto);
	printf("Total de descontos............ %.2f\n", exibeFuncionario.valorDesconto);
	printf("Liquido a receber............. %.2f\n", exibeFuncionario.salarioLiquido);
	printf("_________________________________________________________\n");
	printf("|Pag. 1          Compet�ncia: %d/%d                   |\n", exibeFuncionario.mesPagamento, exibeFuncionario.anoPagamento);
	printf("|_______________________________________________________|\n");
	
	getch();
}
